/*
 *  testGame.c
 *  1917-w123
 *
 *  Created by Richard Buckland on 28/04/11.
 *  Copyright 2011 Licensed under Creative Commons SA-BY-NC 3.0. 
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"

#define CYAN STUDENT_BQN
#define PURP STUDENT_MMONEY
#define YELL STUDENT_MJ
#define RED STUDENT_BPS
#define GREE STUDENT_MTV 
#define BLUE STUDENT_THD

#define DEFAULT_DISCIPLINES {CYAN,PURP,YELL,PURP,YELL,RED ,GREE,GREE, RED,GREE,CYAN,YELL,CYAN,BLUE,YELL,PURP,GREE,CYAN,RED }
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,9,9,2,8,10,5}
void testNewGameBasic (void);
void testGetTurnNumber (void);

int main (int argc, const char * argv[]) {
   testNewGameBasic ();  
   testGetTurnNumber ();  
   printf ("All tests passed!  you are Awesome\n");
   return EXIT_SUCCESS;
}

void testNewGameBasic (void) {
   printf ("testing new Game (basic tests) ...");
   // check makes a game without crashing 
   int disciplines[NUM_REGIONS];
   int dice [NUM_REGIONS];
   
   int regionID = 0;
   while (regionID < NUM_REGIONS) {
      disciplines[regionID] = STUDENT_THD;
      dice[regionID] = 12;
      regionID++;
   }

   Game g = newGame (disciplines, dice);
   assert (getTurnNumber(g) == -1);
   disposeGame (g);
   printf ("Passed\n");
}
                      
void testGetTurnNumber (void) {
   printf ("testing get turn number...");
   int disciplines[] = DEFAULT_DISCIPLINES;
   int dice[] = DEFAULT_DICE;
   Game g = newGame (disciplines, dice);
   
   assert (getTurnNumber(g) == -1);
   disposeGame (g);
   printf ("Passed\n");
}
