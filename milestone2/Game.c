/*
 *  Game.c
 *  1917 v2.0
 *  Add to and change this file as you wish to implement the
 *  interface functions in Game.h
 *
 *  Created by Richard Buckland on 20/04/11.
 *  Copyright 2011 Licensed under Creative Commons SA-BY-NC 3.0.  
 *
 */

#include <stdlib.h>
#include <assert.h>

#include "Game.h"

// your team designs this type not us
// store in this struct all the things you might want to know about
// the game so you can write the interface functions in this header
// eg you might want to store the current turn number (so i've put
// it in for you as an example but take it out if you don;t want it)
typedef struct _game {
   // **you** need to put here the details of the data
   // you want to store in the _game struct
   int currentTurn;
} game;


Game newGame (int discipline[], int dice[]) {
  Game g;
  g = malloc(sizeof(game));
  g->currentTurn = -1;
  return g;
}

void disposeGame (Game g) {
  assert (g != NULL);
  free (g);
}

int getTurnNumber (Game g) {
   return g->currentTurn;
}
